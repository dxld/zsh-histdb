#!/bin/sh
# Upstream doesn't provide git tags or tarballs, so make up commit date
# version numbers.
set -x
gbp import-ref master \
    -u$(git show master -q --format=0.0~git%cd.%h --date=format:%Y%m%d)
